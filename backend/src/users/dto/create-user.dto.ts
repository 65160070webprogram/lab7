import {
  IsEmail,
  IsNotEmpty,
  Length,
  //IsArray,
  //IsString,
  //ArrayNotEmpty,
} from 'class-validator';
export class CreateUserDto {
  // @IsEmail({}, { message: 'Invalid email format' })
  // @IsString({ message: 'Email must be a string' })
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Length(8, 32)
  password: string;

  @IsNotEmpty()
  @Length(5, 32)
  fullName: string;

  // @IsArray()
  // roles: ('admin' | 'user')[];

  @IsNotEmpty()
  gender: 'male' | 'female' | 'others';
}
