import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoadStore = defineStore('loading', () => {
  const isLoading = ref(false)
  const doLoad = () => {
    isLoading.value = true
  }
  const finish = () => {
    isLoading.value = false
  }
  return { isLoading, doLoad, finish }
})
